# Raspberry Pi

- [ ] Buy and check out [Raspberry Pi 3 Model B+](https://www.raspberrypi.org/blog/raspberry-pi-3-model-bplus-sale-now-35/)
- [ ] Check out the Crankshaft OS for the Raspberry Pi

## MPEG-2 and VC-1 hardware decoder patch

- [Reddit source](https://www.reddit.com/r/raspberry_pi/comments/5x7xbo/patch_for_mpeg2_vc1_license/)
- [Hacker News comments](https://news.ycombinator.com/item?id=16379939)

```bash
cd /boot
cp start.elf start.elf_backup && \
perl -pne 's/\x47\xE9362H\x3C\x18/\x47\xE9362H\x3C\x1F/g' < start.elf_backup > start.elf
```

Enables hardware decoded playback of MPEG-2 and VC-1 media without paying for the license.

## Kedei

Raspbery PI + Kedei screen is straight fire.

[The screen review](https://www.raspberrypi.org/forums/viewtopic.php?t=175616)

[Vendor site](http://kedei.net/raspberry/raspberry.html)

## Notes:

- Gotta power both the Raspi and the Kedei or else the screen will keep restarting
- Probably need to use an actual power supply and not the laptop USB lines (not sure what power that is)
- The display native resolution is 480x320 and Raspbian sends 640x480 down which it downscales

## Tasks

- [ ] Try with a dedicated 2.5 A USB charger.
- [ ] Try to enable touch

```sh
sudo apt install xinput-calibrator
sudo nano /boot/config.txt
# Append:
dtparam=spi=on
dtoverlay=ads7846,cs=0,penirq=25,speed=10000,penirq_pull=2,keep_vref_on=1,xohms=150
sudo mkdir -p /etc/X11/xorg.conf.d
DISPLAY=:0.0 xinput_calibrator
# Copy output over to `/etc/X11/xorg.conf.d/99-calibration.conf`
```

*Menu* > *Preferences* > *calibrate Touchscreen*

```
Section "InputClass"
        Identifier      ""
        MatchProduct    ""
        Option  "Calibration"   "# # # #"
        Option  "SwapAxes"      "1"
EndSection
```
